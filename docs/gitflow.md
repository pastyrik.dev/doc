# Git flow

This project is created based on my experience with git and different git flow. It is a simple guide for those who are just starting to use git and git flow or for those who search for some inspiration.

## Git merge vs rebase
I prefer to use rebase instead of merge. It is because rebase is more clear, and it is easier to understand what is going on in the history and no empty merge commits are created. It is also easier to revert changes in the history. Use commit amend or squash to keep the history clean and readable.
It is ideal that one commit contains a specific development that can be rolled back separately. If you need to revert a commit, you can do it easily with rebase. If you need to revert a merge commit, you need to revert all commits that were merged. It is not a problem if you have only one commit in the merge, but if you have more commits, it can be a problem.

## Commit message
This is very important. Commit message should be clear and understandable. It should contain information about what was changed. It is also good to add a ticket number to the commit message. It is easier to find a commit in the history if you need to revert it or if you need to find out why the change was made. For this purpose, I use the following format of the commit message:
```
feat(package where change was made): add polish language JIRA-1234
```
More information about commit message format can be found [here](https://www.conventionalcommits.org/en/v1.0.0/).

## Branch naming
### Development branch
Branch name should contain type of the change and ticket number. For example:
```
feature/JIRA-1234
bugfix/JIRA-1234
```
The type of the change is basically type of the ticket. It can be a feature, bugfix, hotfix, etc. It is good to use the same type of the change as the ticket type. It is easier to find a branch in the repository if you need to revert changes or if you need to find out why the change was made.

### Deployment branch
In my git flow I use 4 environments: dev, test, preprod and prod. I try to explain which branch is used for which environment and why, but the whole release flow is described in the next section.
#### Dev environment
You can use any branch to deploy to this environment. But it is easier to use one dev branch when you have more developers in the team. It is easier to find out what is deployed in the dev environment. It is also easier to find out what is the latest version of the application in the dev environment.
#### Test environment
I use the master branch to deploy to the test environment. It is because I want to test the latest version of the application before it is deployed to the preprod environment. I want to be sure that the latest version of the application is tested before it is deployed to the preprod environment.
#### Preprod environment
When the release is ready to be deployed to the preprod environment, I create a release branch from master branch. All fixes that are made in the release branch are merged back to the master branch.
#### Prod environment
When I have to deploy release changes into production, I create release tag from the release branch. With this approach, I am sure that no one add some changes to the release after all decide that this release is ready to be deployed to production.

In the following diagram, you can see the whole release flow and how the branches are created and merged.
![Branch diagram](images/branches.jpeg)
```mermaid
gitGraph TB:
    commit
    commit
    branch feature/JIRA-1234
    checkout feature/JIRA-1234
    commit
    checkout main
    commit
    commit
    merge feature/JIRA-1234
    commit
    branch release-r1
    checkout release-r1
    commit
    branch bugfix/JIRA-1235
    checkout bugfix/JIRA-1235
    commit
    checkout release-r1
    merge bugfix/JIRA-1235
    checkout main
    merge bugfix/JIRA-1235
    commit
    commit
    checkout release-r1
    commit
    branch bugfix/JIRA-1236
    checkout bugfix/JIRA-1236
    commit
    checkout release-r1
    merge bugfix/JIRA-1236
    checkout main
    merge release-r1
    checkout release-r1
    commit tag: "release-r1"

```

Feature branches are created from master or from release branch. I use release branch, because it is easier to add change into both branches (master and release) at the same time. Or I create feature branch from release when I do not know when the feature should be release. Release branch is merger into master once a day. It is because master contains changes for upcomming releases and you shoul have all changes deployed on production in your master branch. When this is not done, you can easily delete some feature in production with your upcomming release.
The second reason for creating feature branches from release branch is that you can create hotfix after release deployment. Hot fix should be merged into master and release branch. If you create feature branch from master, you have to merge hotfix into master and release branch.