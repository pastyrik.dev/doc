# Project information
site_name: Vojtěch Pastyřík - Doc
site_description: >-
  Here you can find my personal development preferences and documentation of my projects
site_author: Vojtech Pastyřík
site_url: https://gitlab.com/pastyrik.dev/doc

# Repository
repo_name: documentation
repo_url: https://gitlab.com/pastyrik.dev/doc

# Copyright
copyright: 'Copyright &copy; 2024 Vojtěch Pastyřík'

# Configuration
theme:
  name: material
  custom_dir: material/overrides
  logo: images/favicon.png
  favicon: images/favicon.png
  features:
    - announce.dismiss
    - content.code.annotate
    - content.code.copy
    - content.tooltips
    - navigation.footer
    - navigation.indexes
    - navigation.sections
    - navigation.tabs
    - navigation.top
    - navigation.tracking
    - search.highlight
    - search.share
    - search.suggest
    - toc.follow

  palette:
    # Palette toggle for automatic mode
    - media: "(prefers-color-scheme)"
      toggle:
        icon: material/brightness-auto
        name: Switch to light mode

    # Palette toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode

    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to system preference

  # Don't include MkDocs' JavaScript
  include_search_page: false
  search_index_only: true

  # Default values, taken from mkdocs_theme.yml
  language: en
  font:
    text: Roboto
    code: Roboto Mono

# Hooks
hooks:
  - material/overrides/hooks/shortcodes.py
  - material/overrides/hooks/translations.py

# Additional configuration
extra:
  status:
    new: Recently added
    deprecated: Deprecated
  social:
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/Voittok
    - icon: fontawesome/brands/linkedin
      link: https://www.linkedin.com/in/vojt%C4%9Bch-pasty%C5%99%C3%ADk-221a5bab/

# Plugins
plugins:
  - search

# Extensions
markdown_extensions:
  - abbr
  - admonition
  - attr_list
  - def_list
  - footnotes
  - md_in_html
  - toc:
      permalink: true
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.details
  - pymdownx.emoji:
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
      emoji_index: !!python/name:material.extensions.emoji.twemoji
  - pymdownx.highlight:
      anchor_linenums: true
      line_spans: __span
      pygments_lang_class: true
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.magiclink:
      normalize_issue_symbols: true
      repo_url_shorthand: true
      user: squidfunk
      repo: mkdocs-material
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.snippets:
      auto_append:
        - includes/mkdocs.md
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.tabbed:
      alternate_style: true
      combine_header_slug: true
      slugify: !!python/object/apply:pymdownx.slugs.slugify
        kwds:
          case: lower
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde

# Page tree
nav:
  - Home: 'index.md'
  - Documentation:
      - Git: 'gitflow.md'
      - Low-code & No-code: 'low_no_code.md'


