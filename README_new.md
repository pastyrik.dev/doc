# Git Flow

This project is based on my experience with Git and various Git workflows. It serves as a straightforward guide for those who are just starting to use Git and Git workflows or for those seeking some inspiration.

## Git Merge vs. Rebase
I prefer using rebase over merge. This is because rebase provides greater clarity and makes it easier to understand the commit history, without creating empty merge commits. It also simplifies the process of reverting changes in the history. To maintain a clean and readable history, consider using commit amend or squash. Ideally, each commit should represent a specific development that can be rolled back separately. With rebase, reverting a commit is straightforward. In contrast, reverting a merge commit requires undoing all the merged commits. While this isn't a problem with a single commit in the merge, it can become cumbersome with multiple commits.

## Commit Messages
Clear and understandable commit messages are essential. Each message should provide information about what was changed. It's also beneficial to include a ticket number in the commit message. This makes it easier to locate a commit in the history, whether you need to revert it or understand why the change was made. For this purpose, I use the following format for commit messages:
```markdown
feat(package where change was made): add Polish language JIRA-1234
```
You can find more information about the commit message format [here](https://www.conventionalcommits.org/en/v1.0.0/).

## Branch Naming
### Development Branch
Branch names should include the type of change and the associated ticket number. For example:
```
feature/JIRA-1234
bugfix/JIRA-1234
```
The type of change typically corresponds to the type of the ticket, whether it's a feature, bugfix, hotfix, etc. Using the same type of change as the ticket type makes it easier to locate a branch in the repository, whether for reverting changes or understanding the purpose of the change.

### Deployment Branch
In my Git workflow, I use four environments: dev, test, preprod, and prod. I'll explain which branch is used for each environment and why, but the complete release flow is described in the next section.

#### Dev Environment
You can use any branch to deploy to this environment. However, it's easier to use a dedicated dev branch when you have more developers on the team. This makes it easier to track what's deployed in the dev environment and identify the latest version of the application there.

#### Test Environment
I use the master branch for deploying to the test environment. This is because I want to test the latest version of the application before deploying it to the preprod environment. I want to ensure that the latest version of the application is thoroughly tested before it goes to the preprod environment.

#### Preprod Environment
When the release is ready to be deployed to the preprod environment, I create a release branch from the master branch. All fixes made in the release branch are merged back into the master branch.

#### Prod Environment
When I have to deploy release changes into production, I create a release tag from the release branch. This approach ensures that no further changes are added to the release after it's deemed ready for production deploym33ent.

## Release Flow
In the following diagram, you can see the entire release flow and how the branches are created and merged:

![Branch Diagram](images/branches.jpeg)

Feature branches are created from the master or the release branch. I prefer using the release branch because it allows changes to be added to both the master and release branches simultaneously. Alternatively, I create a feature branch from the release branch when I'm unsure about the feature's release date. The release branch is merged into the master branch daily to ensure that the master branch contains changes for upcoming releases. This is crucial to have all changes deployed on production in your master branch; otherwise, upcoming releases could inadvertently delete some features in production.

Another reason for creating feature branches from the release branch is the ability to create hotfixes after release deployment. Hotfixes should be merged into both the master and release branches. If you create a feature branch from the master, you'd need to merge hotfixes into both the master and release branches.

